package com.lynx.bc.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * @Author iLynx
 */

@RunWith(Parameterized.class)
public class TestAuthorEqualsAndHashCode {
    private boolean expectedResult;
    private String[] authorFullNames;

    private static Author author1;
    private static Author author2;

    public TestAuthorEqualsAndHashCode(boolean expectedResult, String[] authorFullNames){
        this.expectedResult = expectedResult;
        this.authorFullNames = authorFullNames;
    }

    @Before
    public void init(){
        author1 = new Author(authorFullNames[0], authorFullNames[1]);
        author2 = new Author(authorFullNames[2], authorFullNames[3]);
    }

    @Parameterized.Parameters
    public static Collection parameters(){
        return Arrays.asList(new Object[][]{
                {true, new String[]{"Tom", "Hardy", "tom", "hardy"}},
                {true, new String[]{"Tom", "Hardy", "tOm", "harDy"}},
                {true, new String[]{"Tom", "Hardy", "TOM", "HARDY"}},
                {false, new String[]{"Tom", "Hardy", "t0m", "harDy"}},
                {false, new String[]{"Tom", "Hardy", "T0M", "HARDY"}},
                {false, new String[]{"Tom", "Hardy", "TomHardy", ""}},
                {false, new String[]{"Tom", "Hardy", "Tom", "Hardy "}},
                {false, new String[]{"Tom", "Hardy", "Dan", "Hardy"}},
                {false, new String[]{"Tom", "Hardy", "Tom", "Watson"}}
        });
    }

    @Test
    public void checkAuthorEquals(){
        boolean actualResult = author1.equals(author2);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void checkHashCode(){
        boolean actualResult = author1.hashCode() == author2.hashCode();
        assertEquals(expectedResult, actualResult);
    }


}
