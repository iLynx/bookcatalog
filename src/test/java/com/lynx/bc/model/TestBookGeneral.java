package com.lynx.bc.model;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * @Author iLynx
 */
public class TestBookGeneral {

    private static Author author;

    @Before
    public void authorInit(){
        author = new Author("Tom", "Hardy");
    }

    @Test
    public void testEmptyConstructor(){
        Book book = new Book();

        assertEquals("id", 0 , book.getId());
        assertEquals("title", null, book.getTitle());
        assertEquals("author", null, book.getAuthor());
        assertEquals("cost",0, book.getCost(),0);
        assertEquals("description", Genre.OTHER, book.getGenre());
    }

    @Test
    public void testNotEmptyConstructor(){
        Book book = new Book("Title", author, Genre.ADVENTURE);

        assertEquals("id", 0 , book.getId());
        assertEquals("title", "Title", book.getTitle());
        assertEquals("author", author, book.getAuthor());
        assertEquals("cost",0, book.getCost(),0);
        assertEquals("description", Genre.ADVENTURE, book.getGenre());
    }

    @Test
    public void testToString(){
        Book book = new Book("Title", author, Genre.ADVENTURE);

        String expectedResult = "\"Title\", Author: "+author;
        assertEquals(expectedResult, book.toString());
    }

    @Test
    public void testToStringEmptySet(){
        Book book = new Book();

        String expectedResult = "\"null\", Author: null";
        assertEquals(expectedResult, book.toString());
    }
}
