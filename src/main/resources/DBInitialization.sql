DROP TABLE book_genres;
DROP TABLE book_catalogue;
DROP TABLE book_authors;

CREATE TABLE book_genres
(
  genre_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  genre VARCHAR(20) NOT NULL
);

INSERT INTO book_genres (genre)
VALUES ('Science_fiction'),
  ('Fantasy'),
  ('Adventure'),
  ('Magazine'),
  ('Other');

CREATE TABLE book_authors
(
  author_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  author_firstName VARCHAR(15) NOT NULL,
  author_lastName VARCHAR(20) NOT NULL
);

INSERT INTO book_authors
(author_firstName, author_lastName)
    VALUES
      ('George','Lucas'),
      ('Suzanne','Collins'),
      ('Herman','Melvill'),
      ('Marvel','Corp'),
      ('from','Ioann');


CREATE TABLE book_catalogue
(
  book_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  book_title VARCHAR(60) NOT NULL,
  author_id INT NOT NULL,
  book_cost DEC(5,2) NOT NULL DEFAULT 0.00,
  book_description BLOB,
  genre_id INT NOT NULL
);

INSERT INTO book_catalogue (book_title,author_id,book_cost,book_description,genre_id)
    VALUES
      ('Moby-Dick', 3 , 251.50 , 'the book about huge white whale that destroying everything', 3),
      ('The Hunger Games', 2 , 150.00, 'lets the game begin!', 2),
      ('Catching Fire', 2 , 155.00, 'Hunger Games sequel, part 2', 2),
      ('The Mockingjay', 2 , 200.00, 'bestseller! HungerGames sequel, part 3', 2),
      ('Star Wars. Episode I. The Phantom Menace', 1 , 280.00, 'The best book ever', 1),
      ('Star Wars. Episode II. Attack of the Clones', 1 , 285.00, 'The best book ever', 1),
      ('Star Wars. Episode III. Revenge of the Sith', 1 , 320.00, 'The best book ever', 1),
      ('Star Wars. Episode IV. A New Hope', 1 , 250.00, 'The best book ever', 1),
      ('Star Wars. Episode V. The Empire Strikes Back', 1 , 250.00, 'The best book ever', 1),
      ('Star Wars. Episode VI. Return of the Jedi', 1 , 250.00, 'The best book ever', 1),
      ('Star Wars. Episode VII. The Force Awakens', 1 , 300.00, 'The best book ever', 1),
      ('Marvel. Wolverine', 4, 15.85, 'Comic book about man with knifes on his arms',4),
      ('Holy Bible', 5, 150.50, 'The first printed book', 5);
