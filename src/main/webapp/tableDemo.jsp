
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.*"%>
<%@ page import="com.lynx.bc.model.Book" %>

<html>
<head>
    <title>Book Catalogue</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
  <h2 align="center">Book Catalogue</h2>
  <br>
  <hr>
  <br>

  <table align="center" border=1>
    <tr>
      <th>Title</th>
      <th>Author</th>
      <th>Genre</th>
      <th>Description</th>
      <th>Cost</th>
    </tr>
    <%
      List<Book> list = (List<Book>) session.getAttribute("booksList");
      for(Book book : list){
    %>
    <tr>
      <th><%=book.getTitle()%></th>
      <th><%=book.getAuthor().toString()%></th>
      <th><%=book.getGenre().toString()%></th>
      <th><%=book.getDescription()%></th>
      <th><%=book.getCost()%></th>
    </tr>
    <%}%>
  </table>

  <a href="index.html">return to start page</a>

</body>
</html>

