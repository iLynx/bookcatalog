package com.lynx.bc.servlets;

import com.lynx.bc.DAO.BookDAO;
import com.lynx.bc.DAO.BookDAOImpl;
import com.lynx.bc.model.Book;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @Author iLynx
 */
public class FirstRequestReceiverServlet extends javax.servlet.http.HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response)
            throws javax.servlet.ServletException, IOException {
        //some code
        String genre = request.getParameter("genre");
        try {

            BookDAO bookDAO = new BookDAOImpl();
            List<Book> booksList = bookDAO.getBooksListByGenre(genre);
            HttpSession session = request.getSession();
            session.setAttribute("booksList", booksList);

            RequestDispatcher rd = getServletContext().getRequestDispatcher("/tableDemo.jsp");
            rd.forward(request, response);
        }catch(IOException e){
            System.out.println("I/O error!");
            System.err.println(e.getMessage());
        }catch(SQLException e2){
            System.out.println("SQLException!");
            System.err.println(e2.getMessage());
        }

    }


}
