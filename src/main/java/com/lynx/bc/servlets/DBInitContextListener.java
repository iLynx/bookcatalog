package com.lynx.bc.servlets;


import com.ibatis.common.jdbc.ScriptRunner;
import com.lynx.bc.util.DBConnector;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;

/**
 * @Author iLynx.
 */

@WebListener()
public class DBInitContextListener implements ServletContextListener {

    // Public constructor is required by servlet spec
    public DBInitContextListener() {
    }


    public void contextInitialized(ServletContextEvent sce) {
        System.err.println("Listener started");
        try {
            // Initialize object for ScripRunner
            ScriptRunner sr = new ScriptRunner(DBConnector.getConnection(), false, false);

            // Give the input file to Reader
            //here should be some pretty clear way to add relative path to the sql-script, but I didn't find it yet =(
            //String path = "/home/lynx/IdeaProjects/BookCatalog/src/main/resources/DBInitialization.sql";  //Linux path
            String path = "C:\\Users\\Игорь\\Documents\\myJavaPrograms\\BookCatalog\\src\\main\\resources\\DBInitialization.sql"; //Windows path
            Reader reader = new BufferedReader(
                    new FileReader(path));
            // Execute script
            sr.runScript(reader);

        } catch (Exception e) {
            System.err.println("Failed to Execute script\n"
                    + " The error is: " + e.getMessage());
        }
    }

    public void contextDestroyed(ServletContextEvent sce) {
      /* This method is invoked when the Servlet Context 
         (the Web application) is undeployed or 
         Application Server shuts down.
      */
    }


}
