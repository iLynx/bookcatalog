package com.lynx.bc.DAO;

import com.lynx.bc.model.Book;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @Author iLynx
 */
public interface BookDAO {

    public Book getBookById(int id) throws IOException, SQLException;
    public void addBook(Book book) throws IOException, SQLException;
    public List<Book> getBooksListByGenre(String genre) throws IOException, SQLException;
}
