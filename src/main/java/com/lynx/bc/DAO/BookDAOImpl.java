package com.lynx.bc.DAO;

import com.lynx.bc.model.Author;
import com.lynx.bc.model.Book;
import com.lynx.bc.model.Genre;
import com.lynx.bc.util.DBConnector;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author iLynx
 */
public class BookDAOImpl implements BookDAO {

    @Override
    public Book getBookById(int id) throws IOException, SQLException {
        Book book = new Book();
        AuthorDAO authorDAO = new AuthorDAOImpl();

        try(Connection connection = DBConnector.getConnection()){
            if(connection == null){
                System.out.println("Have no database connectivity!");
                System.exit(0);
            }
            PreparedStatement stat = connection.prepareStatement("SELECT * FROM book_catalogue WHERE book_id = ?");
            stat.setInt(1,id);
            ResultSet rs = stat.executeQuery();
            while(rs.next()){
                book.setTitle(rs.getString("book_title"));
                book.setAuthor(authorDAO.getAuthorById(rs.getInt("author_id")));
                book.setGenre(getGenreById(rs.getInt("genre_id")));
                book.setDescription(rs.getString("book_description"));
                book.setCost(rs.getDouble("book_cost"));
            }
        }
        return book;
    }

    @Override
    public void addBook(Book book) throws IOException, SQLException{
        AuthorDAO authorDAO = new AuthorDAOImpl();
        try(Connection connection = DBConnector.getConnection()){
            if(connection == null){
                System.out.println("Have no database connectivity!");
                System.exit(0);
            }

            PreparedStatement statement = connection.prepareStatement("INSERT INTO book_catalogue " +
                    "(book_title, author_id, book_cost, book_description, genre_id)" +
                    "VALUES" +
                    "(?,?,?,?,?)");
            statement.setString(1,book.getTitle()); //book_title
            statement.setInt(2, authorDAO.getAuthorIdByName(book.getAuthor().getFirstName(),
                    book.getAuthor().getLastName())); //author_id
            statement.setDouble(3,book.getCost()); //book_cost
            statement.setString(4,book.getDescription()); //book_description
            statement.setInt(5, book.getGenre().getI()); // genre_id - implements genre by id

        }
    }


    public Genre getGenreById(int id) throws IOException, SQLException {
        Genre genre = Genre.OTHER;

        try(Connection connection = DBConnector.getConnection()){
            if(connection == null){
                System.out.println("Have no database connectivity!");
                System.exit(0);
            }

            PreparedStatement ps = connection.prepareStatement("SELECT genre FROM book_genres WHERE genre_id = ?");
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                return Genre.transformGenreFromString(rs.getString("genre"));
            }
        }

        return genre;
    }

    @Override
    public List<Book> getBooksListByGenre(String genre) throws IOException, SQLException {
        List<Book> bookList = new ArrayList<>();
        AuthorDAO authorDAO = new AuthorDAOImpl();

        try(Connection connection = DBConnector.getConnection()){
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM book_catalogue WHERE genre_id = ?");
            ps.setInt(1,Genre.transformGenreFromString(genre).getI());
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Book book = new Book();
                book.setTitle(rs.getString("book_title"));
                book.setAuthor(authorDAO.getAuthorById(rs.getInt("author_id")));
                book.setGenre(getGenreById(rs.getInt("genre_id")));
                book.setDescription(rs.getString("book_description"));
                book.setCost(rs.getDouble("book_cost"));

                bookList.add(book);
            }
        }

        return bookList;
    }

    public static void main(String[] args) throws IOException, SQLException {
        DBConnector.getConnection();
        BookDAO bookDAO = new BookDAOImpl();

        java.util.List list = bookDAO.getBooksListByGenre("fantasy");
        if(list.isEmpty()) {
            System.out.println("list is empty!!");
            System.exit(0);
        }

        for(Object o: list){
            System.err.println(o.toString());
        }

        bookDAO.addBook(new Book("some shit",new Author("Tom", "Hardy"), Genre.OTHER));
        java.util.List list1 = bookDAO.getBooksListByGenre("other");
        if(list1.isEmpty()) {
            System.out.println("list is empty!!");
            System.exit(0);
        }

        for(Object o: list1){
            System.err.println(o.toString());
        }


    }
}
