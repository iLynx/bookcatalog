package com.lynx.bc.DAO;

import com.lynx.bc.model.Author;
import com.lynx.bc.util.DBConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @Author iLynx
 */
public class AuthorDAOImpl implements AuthorDAO {

    public int getAuthorIdByName(String firstName, String lastName){
        int id = 0;

        try(Connection connection = DBConnector.getConnection()){
            PreparedStatement ps = connection.prepareStatement("SELECT author_id FROM book_authors WHERE author_firstName = ? AND author_lastName = ?");
            ps.setString(1, firstName);
            ps.setString(2, lastName);

            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                id = rs.getInt("author_id");
            }

        }catch (Exception e){
            System.err.println(e.getMessage());
        }

        return id;
    }


    public Author getAuthorById(int id){
        Author author = null;

        try(Connection connection = DBConnector.getConnection()){
            PreparedStatement ps = connection.prepareStatement("SELECT author_firstName, author_lastName " +
                    "FROM book_authors WHERE author_id = ?");
            ps.setInt(1,id);

            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                String firstName = rs.getString(1);
                String lastName = rs.getString(2);
                author = new Author(firstName, lastName);
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }


        return author;
    }

    public void addAuthorByName(String firstName, String lastName){

        try(Connection connection = DBConnector.getConnection()){

            PreparedStatement ps1 = connection.prepareStatement("SELECT author_id FROM book_authors WHERE author_firstName = ? AND author_lastName = ?");
            ps1.setString(1, firstName);
            ps1.setString(2, lastName);
            ResultSet rs1 = ps1.executeQuery();
            if(rs1.getObject(1) != null){
                System.out.println("Such author already exists!");
            } else {
                PreparedStatement ps2 = connection.prepareStatement("INSERT INTO book_authors VALUES ('',?,?)");
                ps2.setString(1, firstName);
                ps2.setString(2, lastName);
            }

        }catch(Exception e){
            System.err.println(e.getMessage());
        }
    }
}
