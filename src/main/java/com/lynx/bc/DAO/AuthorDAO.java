package com.lynx.bc.DAO;

import com.lynx.bc.model.Author;

/**
 * @Author lynx
 */
public interface AuthorDAO {
    public int getAuthorIdByName(String firstName, String lastName);
    public Author getAuthorById(int id);
    public void addAuthorByName(String firstName, String lastName);
}
