package com.lynx.bc.util;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @Author iLynx
 */
public class DBConnector {


    public static Connection getConnection() throws SQLException, IOException {

        try (InputStream inputStream = DBConnector.class.getClassLoader().
                getResourceAsStream("connection.properties")){

            Class.forName("com.mysql.jdbc.Driver");
            Properties props = new Properties();
            props.load(inputStream);

            String driver = props.getProperty("jdbc.driver");
            if (driver != null) System.setProperty("jdbc.driver", driver);
            String url = props.getProperty("jdbc.url");
            String userName = props.getProperty("jdbc.userName");
            String password = props.getProperty("jdbc.password");

            return DriverManager.getConnection(url, userName, password);
        } catch (IOException e) {
            System.err.println(e.getMessage());
            return null;   //by default
        } catch (Exception e1) {
            System.err.println(e1.getMessage());
            return null;
        }

    }

}
