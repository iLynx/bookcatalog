package com.lynx.bc.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @Author iLynx
 */
public enum Genre {
    SCIENCE_FICTION(1), FANTASY(2), ADVENTURE(3), MAGAZINE(4), OTHER(5);

    private int i;

    Genre(int i) {
      this.i = i;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }

    public static Genre transformGenreFromString(String sGen) {
        for (Genre g : Genre.values()) {
            if (g.toString().equals(sGen.toLowerCase())) {
                return g;
            }
        }

        return Genre.OTHER;
    }
}

/*class Test {
    public static void main(String[] args) {
        CollectionDS ds = CollectionDS.getInstance();

        ds.addBook(createBook("wiv", "alex", "black", "fantasy"));
        ds.addBook(createBook("hwvvr", "alex", "black", "adventure"));
        ds.addBook(createBook("wiv", "jack", "white", "owjvo"));
        ds.addBook(createBook("owevo", "mike", "white", "adventure"));

        printCollection(ds.listBooks());
        printCollection(ds.listBooksByGenre("adventure"));
        printCollection(ds.authorList());


    }

    public static void printCollection(List list) {
        if (list.isEmpty()) {
            System.out.println("List doesn't contain any data");
        }
        System.out.println("List: " + list.get(0).getClass().getSimpleName());
        for (Object o : list) {
            System.out.println(o);
        }
        System.out.println();
    }

    public static Book createBook(String name, String authorFN, String authorLN, String genre) {
        Book book = new Book();

        book.setTitle(name);
        book.setAuthor(createAuthor(authorFN, authorLN));
        book.setGenre(Genre.transformGenreFromString(genre));

        return book;
    }

    public static Author createAuthor(String firstName, String lastName) {
        Author author = new Author();
        author.setFirstName(firstName);
        author.setLastName(lastName);

        return author;
    }


}



class CollectionDS {
    private List<Book> bookList = new ArrayList<>();
    private List<Author> authorList = new ArrayList<>();

    public static CollectionDS instance;

    private CollectionDS() {

    }
    public static CollectionDS getInstance() {
        if (instance == null) {
            instance = new CollectionDS();
        }

        return instance;
    }

    public void addBook(Book book) {
        if (bookList.contains(book)) {
            return;
        }

        addAuthor(book.getAuthor());

        book.setAuthor(authorList.get(authorList.indexOf(book.getAuthor())));

        bookList.add(book);
    }

    public void addAuthor(Author author) {
        if (authorList.contains(author)) {
            return;
        }

        authorList.add(author);
    }

    public List<Book> listBooks() {
        bookList.sort(new Comparator<Book>() {
            @Override
            public int compare(Book o1, Book o2) {
                return o1.getTitle().compareTo(o2.getTitle());
            }
        });

        return bookList;
    }

    public List<Book> listBooksByGenre(String genre) {
        List<Book> booksResultList = new ArrayList<>();

        for (Book b : bookList) {
            if (b.getGenre().equals(Genre.transformGenreFromString(genre))) {
                booksResultList.add(b);
            }
        }

        return booksResultList;
    }

    public List<Author> authorList() {
        authorList.sort(new Comparator<Author>() {
            @Override
            public int compare(Author o1, Author o2) {
                if (o1.getLastName().equals(o2.getLastName())) {
                    return o1.getFirstName().toLowerCase().compareTo(o2.getFirstName().toLowerCase());
                }
                return o1.getLastName().toLowerCase().compareTo(o2.getLastName().toLowerCase());
            }
        });

        return authorList;
    }
}*/
